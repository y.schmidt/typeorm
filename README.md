Installation
----
Zum Installieren der Pakete `npm install` ausführen

Starten
----
Zum Starten des fertigen Webseerver `npm start` ausführen

Logging
----
Ich habe zum Projekt noch eine Dateii zugefügt, die das Logging übernimmt. Das heißt, es werden Konsolenausgaben in eine Datei und in die Konsole geschrieben. Zum Loggen folgenden Befehl verwenden:

`logger.info('Hier der Text zum loggen')`
