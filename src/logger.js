"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.logger = void 0;
const winston_1 = require("winston");
require("winston-daily-rotate-file");
const moment = require("moment");
const sysLogLevel = 'debug';
const logFormat = winston_1.format.printf(info => {
    return `${info.timestamp} [${info.level}]: ${info.message}`;
});
const timezoned = () => {
    return moment().format();
};
function getRotateFileTransport(filename, level) {
    return new (winston_1.transports.DailyRotateFile)({
        filename: './logs/' + filename + '-%DATE%.log',
        datePattern: 'YYYY-MM-DD',
        zippedArchive: true,
        maxSize: '20m',
        maxFiles: '30d',
        level: level
    });
}
const consoleTransport = (new winston_1.transports.Console({
    format: winston_1.format.combine(winston_1.format.timestamp({ format: timezoned }), logFormat),
    level: sysLogLevel
}));
exports.logger = (0, winston_1.createLogger)({
    level: sysLogLevel,
    format: winston_1.format.combine(winston_1.format.timestamp({ format: timezoned }), logFormat),
    transports: createTransports('app-log', sysLogLevel)
});
function createTransports(filename, level) {
    return [
        getRotateFileTransport(filename, level),
        consoleTransport
    ];
    /*
    switch (process.env.NODE_ENV) {
      case 'test':
      case 'prod':
      case 'dev':
  
      default:
        return [
          consoleTransport
        ];
    }
  
     */
}
