import { transports, format, createLogger } from 'winston';
import 'winston-daily-rotate-file';
import * as moment from 'moment';

const sysLogLevel = 'debug';

const logFormat = format.printf(info => {
  return `${info.timestamp} [${info.level}]: ${info.message}`;
});

const timezoned = () => {
  return moment().format();
};

function getRotateFileTransport (filename: string, level: string) {
  return new (transports.DailyRotateFile)({
    filename: './logs/' + filename + '-%DATE%.log',
    datePattern: 'YYYY-MM-DD',
    zippedArchive: true,
    maxSize: '20m',
    maxFiles: '30d',
    level: level
  });
}

const consoleTransport = (new transports.Console({
  format: format.combine(
    format.timestamp({ format: timezoned }),
    logFormat
  ),
  level: sysLogLevel
}));

export const logger = createLogger({
  level: sysLogLevel,
  format: format.combine(
    format.timestamp({ format: timezoned }),
    logFormat
  ),
  transports: createTransports('app-log', sysLogLevel)
});

function createTransports (filename: string, level: string) {
  return [
    getRotateFileTransport(filename, level),
    consoleTransport
  ];
  /*
  switch (process.env.NODE_ENV) {
    case 'test':
    case 'prod':
    case 'dev':

    default:
      return [
        consoleTransport
      ];
  }

   */
}
