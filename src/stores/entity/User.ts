import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

/**
 * Ein Klasse für User
 * Durch @Entity() wird mit dem Paket typeorm eine Tabelle in der Datenbank erstellt.
 * @Column gibt eine Tabellenspalte an
 * Genaueres findest du hier: https://typeorm.io/entities
 */
@Entity()
export class User {
    @PrimaryGeneratedColumn()
    id: string;

    @Column()
    firstName: string;

    @Column()
    lastName: string;

    @Column()
    mail: string;
}
