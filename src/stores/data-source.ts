import { DataSource } from 'typeorm';

/**
 * Hier werden die Verbindungsdaten zur Datenbank bereit gestellt
 */
export const AppDataSource = new DataSource({
    type: "mariadb",
    host: "localhost",
    port: 3306,
    username: "peter",
    password: "1234",
    database: "peter",
    synchronize: true,
    logging: false,
    entities: ["src/stores/entity/*.js"],
})
