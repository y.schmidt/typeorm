"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppDataSource = void 0;
const typeorm_1 = require("typeorm");
/**
 * Hier werden die Verbindungsdaten zur Datenbank bereit gestellt
 */
exports.AppDataSource = new typeorm_1.DataSource({
    type: "mariadb",
    host: "localhost",
    port: 3306,
    username: "peter",
    password: "1234",
    database: "peter",
    synchronize: true,
    logging: false,
    entities: ["src/stores/entity/*.js"],
});
