"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const app = express(); // Erzeugt express Objekt aus dem express Paket
const port = 3000; // Legt Port für Webserver fest
app.get('/', (req, res) => {
    res.send('hello World'); // Sendet eine Antwort auf die Anfrage. Konkret wird hier der string Hello World zurück gegeben.
});
app.get('/uhrzeit', (req, res) => {
    res.send(Date.now().toLocaleString());
});
app.get('/level/:levelId', (req, res) => {
    console.log(req.params); // Greift auf alle Variablen der Anfrage zu
    res.send(req.params.levelId); // Schickt eine Antwort mit der LevelId zurück
});
app.listen(port, () => {
    console.log(`Example app listening on port ${port}`);
});
