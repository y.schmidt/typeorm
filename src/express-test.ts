import * as express from 'express';
const app = express();  // Erzeugt express Objekt aus dem express Paket
const port = 3000;      // Legt Port für Webserver fest

app.get('/', (req, res) => {    // Erzeugt eine Route für den Webserver. URL: localhost
    res.send('hello World');    // Sendet eine Antwort auf die Anfrage. Konkret wird hier der string Hello World zurück gegeben.
})

app.get('/uhrzeit', (req, res) => { // Erzeugt eine Route für den Webserver. URL: localhost/uhrzeit
    res.send(Date.now().toLocaleString());
})

app.get('/level/:levelId', (req, res) => {  // Erzeugt eine Route mit der URL localhost/level/:levelId. Die levelId ist eine Variable
    console.log(req.params);                // Greift auf alle Variablen der Anfrage zu
    res.send(req.params.levelId);           // Schickt eine Antwort mit der LevelId zurück
})

app.listen(port, () => { // Startet den Webserver
    console.log(`Example app listening on port ${port}`)
});
