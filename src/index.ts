import 'reflect-metadata';
import { AppDataSource } from './stores/data-source';
import { User } from './stores/entity/User';
import * as express from 'express';
import { logger } from './logger';
import * as util from 'util';


AppDataSource.initialize()                   // Stellt die Verbindung zur Datenbank her
    .then(async (dataSource) => { // "then" wird ausgeführt, wenn die Verbindung steht, falls etwas fehlschlägt wird unten zu "catch" gegangen

        const app = express(); // Erstellt wieder ein Webserver Objekt

        /**
         * Diese so genannte Middelware wird bei jeder Anfrage ausgeführt und übergibt an den nächsten Punkt
         * Hier wird lediglich in die Konsole geschrieben, dass eine Anfrage stattgefunden hat
         */
        app.use((req, res, next) => {
            logger.info(`Request on URL [${req.method}]:  ${req.originalUrl}`); // Schreibt den Text ins Log und in die Konsole
            next(); // Gibt die Anfrage an den nächsten Punkt wei
        });

        /**
         * Eine Route mit Pfad localhost/user
         * Die Route holt alle Benutzer aus der Datenbank und gibt sie zurück
         */
        app.get('/user', async (req, res) => {
            const user = await dataSource.getRepository(User).find();
            res.send(user);
        });

        /**
         * Eine Route mit Pfad localhost/user/userId
         * Die Route holt den Benutzer mit der übergebenen ID aus der Datenbank und gibt ihn zurück
         */
        app.get('/user/:userId', async (req, res) => {
            const user = await dataSource.getRepository(User).findBy({
                id: req.params.userId
            });
            res.send(user);
        });

        /**
         * App (Webserver) wird gestartet
         */
        app.listen(3000, () => {
            logger.info(`Example app listening on port 3000`);
        });

    })
    .catch((error) => {     // wird nur ausgeführt, wenn ein Fehler auftritt bei der Verbindung zur Datenbank
        logger.error(util.format(error)); // schreibt den Fehler in die Konsole, util.format gibt das error-Objekt lesbar aus
    });
