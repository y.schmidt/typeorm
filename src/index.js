"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
require("reflect-metadata");
const data_source_1 = require("./stores/data-source");
const User_1 = require("./stores/entity/User");
const express = require("express");
const logger_1 = require("./logger");
const util = require("util");
data_source_1.AppDataSource.initialize() // Stellt die Verbindung zur Datenbank her
    .then((dataSource) => __awaiter(void 0, void 0, void 0, function* () {
    const app = express(); // Erstellt wieder ein Webserver Objekt
    /**
     * Diese so genannte Middelware wird bei jeder Anfrage ausgeführt und übergibt an den nächsten Punkt
     * Hier wird lediglich in die Konsole geschrieben, dass eine Anfrage stattgefunden hat
     */
    app.use((req, res, next) => {
        logger_1.logger.info(`Request on URL [${req.method}]:  ${req.originalUrl}`); // Schreibt den Text ins Log und in die Konsole
        next(); // Gibt die Anfrage an den nächsten Punkt wei
    });
    /**
     * Eine Route mit Pfad localhost/user
     * Die Route holt alle Benutzer aus der Datenbank und gibt sie zurück
     */
    app.get('/user', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        const user = yield dataSource.getRepository(User_1.User).find();
        res.send(user);
    }));
    /**
     * Eine Route mit Pfad localhost/user/userId
     * Die Route holt den Benutzer mit der übergebenen ID aus der Datenbank und gibt ihn zurück
     */
    app.get('/user/:userId', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        const user = yield dataSource.getRepository(User_1.User).findBy({
            id: req.params.userId
        });
        res.send(user);
    }));
    /**
     * App (Webserver) wird gestartet
     */
    app.listen(3000, () => {
        logger_1.logger.info(`Example app listening on port 3000`);
    });
}))
    .catch((error) => {
    logger_1.logger.error(util.format(error)); // schreibt den Fehler in die Konsole, util.format gibt das error-Objekt lesbar aus
});
